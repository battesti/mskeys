<?php
    session_start();
    include "fonctionDB.php";
    
    $connexion = connect();
    sessionConnexion($connexion);
        
    $action = $_GET['action'];
    if(isset($_GET['id'])){
        $id=$_GET['id'];
    }
    if(isset($_POST['nom']))
        $nom=$_POST['nom'];
        
    
    switch($action)
    {
        case'modif':
            modifOS($connexion,$id,$nom);
            break;
        case 'suppr':
            supprimOS($connexion, $id);
            break;
        case 'ajout':
            ajoutOS($connexion,$nom);
            break;
    }
    header("Location: gestOS.php");
        
?>
