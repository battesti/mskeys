<!DOCTYPE html>
<html lang="fr">
    <?php
        session_start();
        include "fonctionDB.php";
        
        $connexion = connect();
        sessionConnexion($connexion);
        
        if(isset($_GET["nom"]))
            $nom=$_GET["nom"];
        
        $tab_os=recupOS($connexion);
        $action=$_GET["action"];
        if(isset($_GET["id"]))
            $id=$_GET["id"];
        
    ?>
 
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="MSDN LLB Keys">
    <meta name="author" content="LLB">
    <link rel="shortcut icon" href="favicon.ico">
    <title>MSKeys LLB</title>
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    
    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">MSKeys LLB</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Accueil</a></li>
                <li ><a href="importations.php">Importations</a></li>
                <li ><a href="gestOS.php">Gestion d'OS</a></li>
                <li ><a href="prefCompte.php">Paramètres</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
      <div class="jumbotron">
            <form name="formOS" action="actionOS.php?action=<?php echo $action; ?>&id=<?php if($action=="modif"){echo $id;} ?>" method="POST">
                <center>Nom : 
                    <input type="text" name="nom" size="80" value="<?php if($action=="modif"){echo $nom;} ?>">
                    <br>
                </center>
                
                <input type="submit" name="Envois" value="<?php if($action=='modif'){echo 'Modifier';}else {echo 'Ajouter';} ?>"/>
                <BQ></BQ><a href="gestOS.php"><button type="button">Retour</button></a>
            </form>
      </div>
  </body>   